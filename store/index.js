import { Store } from 'vuex'

const createStore = () => {
  return new Store({
    state: {
      screen: {},
      catalog: {},
      product: {},
      cart: {},
    },
    getters: {
      screen: (state) => state.screen,
    },
    mutations: {
      screen: (state, value) => {
        state.screen = value
      },
      setCart(state, cart) {
        state.cart = cart
      },
      setCatalog(state, catalog) {
        state.catalog = catalog
      },
      setProduct(state, product) {
        state.product = product
      },
    },
    actions: {
      screen: ({ state }, value) => {
        state.screen = value
      },
      setCart: ({ commit }, cart) => {
        commit('setCart', cart)
      },
    },
  })
}

export default createStore
