export default function (req, res, next) {
  const redirects = [
    {
      from: '/',
      to: 'https://landing.chadiart.com',
    },
  ]
  const redirect = redirects.find((r) => r.from === req.url)
  if (redirect) {
    res.writeHead(200, { Location: redirect.to })
    res.end()
  } else {
    next()
  }
}
