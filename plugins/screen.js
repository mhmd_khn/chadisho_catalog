
export default function ({ store }) {
  const cols = {
    xs: 576,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1199,
  }

  const handleChangeScreen = () => {
    let col

    const screen = {
      width: window.innerWidth,
      height: window.innerHeight,
      heightOuter: window.outerHeight,
    }

    if (window.innerWidth < cols.xs) col = 'xs'
    if (window.innerWidth > cols.sm) col = 'sm'
    if (window.innerWidth > cols.md) col = 'md'
    if (window.innerWidth > cols.lg) col = 'lg'
    if (window.innerWidth > cols.xl) col = 'xl'

    screen.col = col
    screen.width = window.innerWidth
    screen.height = window.innerHeight
    screen.heightOuter = window.outerHeight
    screen.isMobile = window.innerWidth <= cols.sm

    store.dispatch('screen', screen)
  }

  handleChangeScreen()

  window.addEventListener('resize', handleChangeScreen)
}
