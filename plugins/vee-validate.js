import Vue from 'vue'

import {
  ValidationProvider,
  ValidationObserver,
  localize,
  extend,
} from 'vee-validate/dist/vee-validate.full'

// import { required } from 'vee-validate/dist/rules'

import fa from 'vee-validate/dist/locale/fa.json'

localize({
  fa: {
    messages: fa.messages,
    names: {
      email: 'ایمیل',
    },
  },
})

// extend('irani', {
//   validate: (code) => {
//     const L = code.length
//     if (L !== 10 || parseInt(code, 10) === 0) return false
//     code = ('0000' + code).substr(L + 4 - 10)
//     if (parseInt(code.substr(3, 6), 10) === 0) return false
//     const c = parseInt(code.substr(9, 1), 10)
//     const s = 0
//     for (const i = 0; i < 9; i++) s += parseInt(code.substr(i, 1), 10) * (10 - i)
//     s = s % 11
//     return (s < 2 && c === s) || (s >= 2 && c === 11 - s)
//   },
//   message: 'کد ملی صحیح نمی‌باشد.',
// })

extend('mobile', {
  validate: (value) => {
    return /^((0)(9){1}[0-9]{9})+$/.test(value)
  },
  message: 'شماره سیم‌کارت صحیح نمی‌باشد',
})

localize('fa', fa)

Vue.component('v-validate', ValidationProvider)
Vue.component('v-validate-observer', ValidationObserver)
