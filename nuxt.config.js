import colors from 'vuetify/es5/util/colors'
import config from './config'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - main_chadisho',
    title: 'main_chadisho',
    htmlAttrs: {
      lang: 'fa',
      dir: 'rtl',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~assets/scss/global.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/axios',
    { src: '~/plugins/directives', mode: 'client' },
    { src: '~/plugins/screen', mode: 'client' },
    '~/plugins/vee-validate.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/composition-api/module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth',
  ],
  // nuxt auth
  auth: {
    redirect: {
      login: '/login',
      logout: false,
      callback: '/login',
      home: false,
    },
    strategies: {
      local: {
        endpoints: {
          login: false,
          user: false,
          logout: {
            url: '/logout',
            method: 'post',
          },
        },
        autoFetchUser: false,
        tokenType: 'Bearer',
      },
    },
    plugins: ['~/plugins/auth'],
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: config.baseURL,
    headers: {
      common: {
        apiToken:
          'f4cdc28ed938b4abdc75efe154d5a3a81f006f454f1376f1191bcd0c20455104',
      },
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    rtl: true,
    customVariables: ['~/assets/scss/variables.scss'],
    theme: {
      dark: true,
      themes: {
        light: {
          primary: '#9E19FF',
          secondary: colors.grey.darken3,
          accent: colors.blue.darken2,
          base: colors.shades.black,
          background: '#000',
        },
        dark: {
          primary: '#9E19FF',
          secondary: colors.grey.lighten3,
          accent: colors.blue.lighten2,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          base: colors.shades.white,
          background: '#fff',
        },
      },
    },
  },
  serverMiddleware: ['~/middleware/redirects.js'],
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['vee-validate/dist/rules'],
  },
}
