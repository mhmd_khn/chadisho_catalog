import { computed, ref } from '@nuxtjs/composition-api'
import useGlobal from '~/composables/useGlobal'

const useProduct = (product) => {
  const { calculateDiscount } = useGlobal()

  const attributeProductId = ref([])

  const productInfo = computed(() => {
    if (!product) return {}

    let response = {
      discount: calculateDiscount(product.value.product),
      ...product.value.product,
    }

    product.value.variations.forEach((variation) => {
      const check = []
      variation.attribute_product_ids.forEach((item, index) => {
        if (attributeProductId.value.includes(Number(item))) {
          check[index] = true
        } else {
          check[index] = false
        }
      })
      if (!check.includes(false)) {
        response = {
          discount: calculateDiscount(variation),
          currency: product.value.product.currency,
          unit: product.value.product.unit,
          ...variation,
        }
      }
    })

    return response
  })

  const canAddToCart = computed(() => {
    if (!productInfo.value) return false
    if (productInfo.value && productInfo.value.stock_type === 'notInStock')
      return false
    if (productInfo.value && productInfo.value.stock_type === 'limited') {
      if (productInfo.value.quantity > 0) {
        return true
      } else return false
    }
    return true
  })

  const variations = computed(() => {
    const variations = []
    if (product) {
      product.value.variation_attributes.forEach((element) => {
        if (element.values[0].is_variation) {
          variations.push(element)
        }
      })
    }
    return variations
  })

  const attributes = computed(() => {
    const attributes = []
    if (product) {
      product.value.variation_attributes.forEach((element) => {
        if (!element.values[0].is_variation) {
          attributes.push(element)
        }
      })
    }
    return attributes
  })

  return {
    product,
    productInfo,
    canAddToCart,
    variations,
    attributes,
    attributeProductId,
  }
}

export default useProduct
