import { h } from 'vue'
import { computed, defineComponent } from '@nuxtjs/composition-api'
import useScreen from '@/composables/useScreen'

export default () => {
  const screen = useScreen()

  const prefixName = computed(() =>
    screen.value.isMobile ? 'mobile' : 'desktop'
  )

  const customComponent = defineComponent({
    inheritAttrs: false,
    props: {
      component: String,
    },
    render() {
      return h('component', {
        props: {
          ...this.$attrs,
        },
        is: `${prefixName.value}-${this.$props.component}`,
      })
    },
  })

  return customComponent
}
