import { useContext } from '@nuxtjs/composition-api'

const useHttp = () => {
  const { $axios } = useContext()

  const get = async ({ url, options }) => {
    return await $axios.$get(url, options)
  }

  const post = async ({ url, body, options }) => {
    return await $axios.$post(url, body, options)
  }

  return { post, get }
}

export default useHttp
