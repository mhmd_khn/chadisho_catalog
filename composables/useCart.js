import { computed, ref, useStore } from '@nuxtjs/composition-api'
import useGlobal from './useGlobal'
import useHttp from './useHttp'
import useSwal from './useSwal'

const useCart = () => {
  const store = useStore()
  const { post } = useHttp()
  const { handleError } = useGlobal()

  const isLoadingAddToCart = ref(false)
  const isLoadingGetCart = ref(false)
  const isLoadingUpdateCartItem = ref(false)
  const isLoadingDeleteCartItem = ref(false)
  const isLoadingNewOrder = ref(false)

  const cart = computed(() => store.state.cart)

  const addToCart = (product) => {
    isLoadingAddToCart.value = true

    post({ url: 'app/cart/add-to-cart', body: product })
      .then((res) => {
        if (res.status === 200) {
          useSwal({
            title: 'محصول به سبد خرید اضافه شد',
          })
          getCart()
        } else {
          useSwal({
            icon: 'error',
            title: res.message,
          })
        }
        return res
      })
      .catch((err) => {
        handleError(err)
      })
      .finally(() => {
        isLoadingAddToCart.value = false
      })
  }

  const getCart = () => {
    isLoadingGetCart.value = true

    post({ url: 'app/cart' })
      .then((res) => {
        if (res.status === 200) {
          const cart = res.options?.cart || {}

          store.dispatch('setCart', cart)
        } else {
          useSwal({
            icon: 'error',
            title: res.message,
          })
        }
      })
      .catch((err) => {
        handleError(err)
      })
      .finally(() => {
        isLoadingGetCart.value = false
        isLoadingUpdateCartItem.value = false
        isLoadingDeleteCartItem.value = false
      })
  }

  const updateCartItem = (orderItemID, quantity) => {
    isLoadingUpdateCartItem.value = true

    post({
      url: `app/cart/update-cart-item/${orderItemID}`,
      body: { quantity },
    })
      .then((res) => {
        if (res.status === 200) {
          useSwal({
            title: 'تعداد محصول ویرایش شد',
          })
          getCart()
        } else {
          useSwal({
            icon: 'error',
            title: res.message,
          })
        }
        return res
      })
      .catch((err) => {
        handleError(err)
      })
  }

  const deleteCartItem = (orderItemID) => {
    isLoadingDeleteCartItem.value = true

    post({ url: `app/cart/delete-cart-item/${orderItemID}` })
      .then((res) => {
        if (res.status === 200) {
          useSwal({
            title: 'محصول از سبد خرید حذف شد',
          })
          getCart()
        } else {
          useSwal({
            icon: 'error',
            title: res.message,
          })
        }
        return res
      })
      .catch((err) => {
        handleError(err)
      })
  }

  const newOrder = () => {
    isLoadingNewOrder.value = true

    post({ url: `app/orders/new-order/${cart.value.id}` })
      .then((res) => {
        if (res.status === 200) {
          useSwal({
            title: 'سفارش شما با موفقیت ثبت شد',
          })
          getCart()
        } else {
          useSwal({
            icon: 'error',
            title: res.message,
          })
        }
        return res
      })
      .catch((err) => {
        handleError(err)
      })
      .finally(() => {
        isLoadingNewOrder.value = false
      })
  }

  const calculateOrderPrice = computed(() => {
    if (!cart.value.order_items) return
    const price = {
      price: 0,
      salePrice: 0,
    }
    cart.value.order_items.forEach((item) => {
      if (item.product_price.price > 0) {
        price.price += Number(item.product_price.price * item.quantity)
      }
      if (item.product_price.sale_price > 0) {
        price.salePrice += Number(item.product_price.sale_price * item.quantity)
      } else {
        price.salePrice += Number(item.product_price.price * item.quantity)
      }
    })
    return price
  })

  return {
    cart,
    addToCart,
    getCart,
    updateCartItem,
    deleteCartItem,
    isLoadingAddToCart,
    isLoadingGetCart,
    isLoadingUpdateCartItem,
    isLoadingDeleteCartItem,
    isLoadingNewOrder,
    newOrder,
    calculateOrderPrice,
  }
}

export default useCart
