import { computed, useStore } from '@nuxtjs/composition-api'

const screen = () => {
  const store = useStore()

  const getScreen = computed(() => store.state.screen)

  return getScreen
}

export default screen
