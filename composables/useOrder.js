import { computed, ref } from '@nuxtjs/composition-api'
import collect from 'collect.js'
import useHttp from './useHttp'
import useSwal from './useSwal'

const useOrder = () => {
  const { post } = useHttp()

  const isLoading = ref(false)
  const isLoadingOrderItems = ref(false)

  const orders = ref()
  const orderItems = ref()

  const getOrders = () => {
    isLoading.value = true

    post({ url: 'app/orders/index' })
      .then((res) => {
        if (res.status === 200) {
          orders.value = res.options?.orders || {}
          if (orders.value.data.length) {
            getOrderDetails(orders.value.data[0].id)
          }
        } else {
          useSwal({
            icon: 'error',
            title: res.message,
          })
        }
      })
      .catch((err) => {
        useSwal({
          icon: 'error',
          title: err?.response?.data?.message,
        })
      })
      .finally(() => {
        isLoading.value = false
      })
  }
  const getOrderDetails = (orderID) => {
    isLoadingOrderItems.value = true
    orderItems.value = []
    post({ url: `app/orders/show/${orderID}` })
      .then((res) => {
        if (res.status === 200) {
          orderItems.value = res.options?.order_items || {}
        } else {
          useSwal({
            icon: 'error',
            title: res.message,
          })
        }
      })
      .catch((err) => {
        useSwal({
          icon: 'error',
          title: err?.response?.data?.message,
        })
      })
      .finally(() => {
        isLoadingOrderItems.value = false
      })
  }

  const groupedOrders = computed(() => {
    return collect(orders.value.data).groupBy('status').all()
  })

  return {
    orders,
    orderItems,
    getOrders,
    getOrderDetails,
    isLoading,
    isLoadingOrderItems,
    groupedOrders,
  }
}

export default useOrder
