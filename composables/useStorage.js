import config from '@/config'

const useStorage = (key) => {
  const setItem = (value) => {

    if (typeof window !== 'undefined') {
      if (value) value = JSON.stringify(value)

      localStorage.setItem(config.perfix + key, value)
    }
  }

  const getItem = () => {
    if (typeof window !== 'undefined') {
      let value = localStorage.getItem(config.perfix + key)

      if (value) value = JSON.parse(value)

      return value
    }

    return false
  }

  return [setItem, getItem]
}

export default useStorage
