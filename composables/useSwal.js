import swal from 'sweetalert2'

const useSwal = (options) => {
  swal.fire({
    toast: true,
    position: 'bottom-right',
    icon: 'success',
    title: 'Your work has been saved',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    ...options,
  })
}

export default useSwal
