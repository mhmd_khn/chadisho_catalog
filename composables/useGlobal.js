import { computed, useRouter, useStore } from '@nuxtjs/composition-api'
import moment from 'moment-jalaali'
import useSwal from './useSwal'

const useGlobal = () => {
  const store = useStore()
  const router = useRouter()

  const calculateDiscount = (product) => {
    if (product.sale_price > 0) {
      return Number(
        (((product.price - product.sale_price) / product.price) * 100).toFixed(
          2
        )
      )
    } else return 0
  }

  const titleToSlug = (title) => {
    return title ? title.replace(/\s+/g, '-').toLowerCase() : 'title'
  }

  const separateNum = (num, sep = ',', string) => {
    if (num) {
      const number = typeof num === 'number' ? num.toString() : num

      return number.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + sep)
    } else {
      return ''
    }
  }

  const toEnglishDigits = (str) => {
    // convert persian digits [۰۱۲۳۴۵۶۷۸۹]
    let e = '۰'.charCodeAt(0)
    str = str.replace(/[۰-۹]/g, function (t) {
      return t.charCodeAt(0) - e
    })

    // convert arabic indic digits [٠١٢٣٤٥٦٧٨٩]
    e = '٠'.charCodeAt(0)
    str = str.replace(/[٠-٩]/g, function (t) {
      return t.charCodeAt(0) - e
    })
    return str
  }

  const getImage = (image) => {
    if (image) {
      return image
    } else return require('../assets/images/default.png')
  }

  const getDateTime = (dateTime = null, format = 'jYYYY/jMM/jDD HH:mm:ss') => {
    try {
      if (dateTime)
        return moment(dateTime, 'YYYY-MM-DD HH:mm:ss').format(format)
      return moment().format(format)
    } catch (err) {
      console.log('Error in getJalaliDateTime', dateTime)
    }
  }

  const showPrice = computed(() => {
    const catalog = store.state?.catalog?.catalog
    if (catalog) {
      return catalog.show_product_price_status === 'active'
    } else return true
  })

  const getPirceCurrency = (currency) => {
    switch (currency) {
      case 'toman':
        return 'تومان'
      case 'usd':
        return '$'
      case 'pound':
        return '£'
      case 'lir':
        return '₺'
      case 'uae':
        return 'د.إ'
      case 'eur':
        return '€'

      default:
        break
    }
  }

  const handleError = (error) => {
    if (error.response.status === 401) {
      useSwal({
        icon: 'error',
        title: 'لطفا وارد حساب کاربری خود شوید',
      })
      router.push({ name: 'login' })
    } else {
      useSwal({
        icon: 'error',
        title: error.response.data.message,
      })
    }
  }

  return {
    calculateDiscount,
    titleToSlug,
    separateNum,
    toEnglishDigits,
    getImage,
    getDateTime,
    showPrice,
    getPirceCurrency,
    handleError,
  }
}

export default useGlobal
